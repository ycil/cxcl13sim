This repository contains LymphSim, a 3D simulation of a lymph node cortex. Includes collisions, chemotaxis, random movement and diffusion.


Uses:

* Mason v19 (required)
* jzy3d 0.9.1 (optional)

Notes: 

1) Mac users are required to have Java 6, due to compatibility issues between later java versions and java3d libraries.

2) The GUI should be run with low cell numbers due to the high memory requirements of visualisation

3) (optional) dependencies are not required to run the simulation. (required) dependencies will cause the simulation to crash when missing, but some may be able to be disabled.


Created by Jason Cosgrove (jason.cosgrove@york.ac.uk) and Simon Jarrett (simonjjarrett@gmail.com)# CXCL13Sim
